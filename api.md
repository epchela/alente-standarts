```ts
type PageWithMeta = {
	meta: {
		meta_title: string;
		meta_description: string;
		meta_keywords: string;
		og_title: string;
		og_description: string;
		og_image: string | null;
	},
	...rest
}

// Filter
type FilterParam = {
	name: string,
	type: 'list' | 'list-radio' | 'range'
	values: {
		name: string,
		value: string
	}[] | [number, number] // [min, max]
}

type FilterParams = FilterParam[]

// request with filter params
// каждый параметр передается как есть, то есть в виде массива (дефолтный формат передачи чекбоксом через FormData)
// так что тут без чего то уникального

// Сортировка
// Привести к одинаковым названиям (это лучше бекендерам делать)
// Например сортировка по названию:
const PRICE_KEY = 'price',
const PRICE_ASC = 'price',
const PRICE_DESC = '-price',
// asc и desc можно сделать более стандартными - например так и передавать asc и desc (вместо price/-price)

// Common (пример, потому что может от проекта к проекту отличаться)
type Common = {
	header: Header;
	footer: Footer;
	socials: Social[] | undefined;
	cookie: Cookie;
	scripts: Scripts;
};

type Header = {
	logo: {
		src: string,
		alt: string,
	},
	// если в шапке есть эта инфа
	contacts: {
		phone: string;
	    email: string;
	    address: string;
	},
    menu: Menu[];
}

export type Menu = {
  name: string;
  slug: string;
  children: Menu[]
};

type Footer = {
	logo: {
		src: string,
		alt: string,
	},
	contacts: {
		phone: string;
	    email: string;
	    address: string;
	},
    copyright: string;
    menu: Menu[];
};

type Social = {
  logo: string;
  alt: string;
  link: string;
};

export type Cookie = {
    text: string;
    agree_text: string;
    disagree_text: string;
};

export type Scripts = string[]
	| {
		type: 'head' | 'bodyBegin' | 'bodyEnd', 
		content: string
	  }[]

```